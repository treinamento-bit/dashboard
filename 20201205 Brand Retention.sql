with
--
microzones as (
select distinct
                s.store_id,
                mz.name as microzone_name,
                z.microzone_id as microzone_id,
                ca.city,
                replace(r.region, ' ', '') as region,
                s.exclusive as exclusivity
from br_grability_public.stores s
left join (
        select distinct
                o.store_id,
                coalesce(zl.micro_zone_id,mz.store_microzone_id) as microzone_id,
                zl.city_address_id
        from global_finances.br_orders o
        left join (
            select distinct
                    o.store_id,
                    last_value(o.microzone_id) over (partition by o.store_id order by o.created_at asc) as store_microzone_id
            from global_finances.br_orders o
            where o.order_state in ('finished', 'pending_review')
            and o.vertical = 'RESTAURANTES') mz on mz.store_id = o.store_id
        left join br_writable.zone_store_last zl on zl.store_id = o.store_id
        where o.created_at :: date >= '2020-01-01') z on z.store_id = s.store_id
        left join br_grability_public.city_addresses ca
        on ca.id = coalesce(z.city_address_id,s.city_address_id) and coalesce(ca._fivetran_deleted, false) = false
        left join br_pg_bibr_public.br_regions r
        on r.city = ca.city and coalesce(r._fivetran_deleted, false) = false
        left join br_grability_public.micro_zones mz
        on mz.id = z.microzone_id and coalesce(mz._fivetran_deleted, false) = false
        where coalesce(s._fivetran_deleted,false) = false
),
--
calendar as (
select 
	dateadd(month,- row_number() over (order by (select 1)), 
	date_trunc(month, current_date)) as calendar 
from table(generator(rowcount => 50))
union all
select
	date_trunc(month, current_date) as calendar 
order by 1 desc
),
--
stores_attribution as (
select distinct
    store_id
    ,tier
    ,sector
    ,commercial_email
    ,leader_email
    ,is_exclusive
from BR_WRITABLE.post_sales_structure_rest pss
left join br_writable.portfolio_stores ps on ps.portfolio_id = pss.portfolio_id and ps.ref_month = pss.ref_month
where ps.ref_month = date_trunc(month, dateadd(day, -1, current_date))
),
--
users as (
select distinct 
	o.application_user_id,
	date_trunc(month, o.created_at :: date) as calendar,
	mz.city,
	o.brand_id,
	o.brand_name,
	coalesce(sa.is_exclusive,false) as is_exclusive,
	count(distinct o.order_id) as orders,
	sum(o.gmv) / 4.45 as gmv
from global_finances.br_orders o
left join microzones mz on mz.store_id = o.store_id
left join stores_attribution sa on sa.store_id = o.store_id
where 1=1
	and o.vertical = 'RESTAURANTES'
	and o.count_to_gmv
--	and year(o.created_at) in (2019,2020)
	and (o.created_at :: date) <> current_date
--	and o.type_prime_subscription in ('RENOVATE','TRIAL','FIRST','NEW?')
	and mz.city in ('Brasilia','Belo Horizonte','Campinas','Curitiba','Fortaleza','Grande São Paulo','Porto Alegre','Recife','Rio de Janeiro')
	and sa.tier in ('Middle','VIP')
--	and sa.tier in ('EAM')
group by 1,2,3,4,5,6
having 1=1
),
--
aux as (
select distinct	
	c.calendar,
	u.application_user_id,
	u.brand_id,
	sum(orders) as orders
from calendar c
cross join users u
where 1=1
	and u.calendar < c.calendar
group by 1,2,3
order by 2, 1 desc
),
--
base as (
select distinct
	u0.city,
	u0.application_user_id,
	u0.brand_id,
	u0.brand_name,
	u0.is_exclusive,
	u0.calendar,
	u0.gmv as gmv0,
	u0.orders as orders0,
	u1.orders as orders1,
	u2.orders as orders2,
	a.orders as orders_acc
from users u0
left join users u1 on u1.application_user_id = u0.application_user_id and u1.brand_id = u0.brand_id and u1.calendar = u0.calendar - interval '1 month'
left join users u2 on u2.application_user_id = u0.application_user_id and u2.brand_id = u0.brand_id and u2.calendar = u0.calendar - interval '2 month'
left join aux a on a.application_user_id = u0.application_user_id and a.calendar = u0.calendar and a.brand_id = u0.brand_id
where 1=1
order by 1, 3 desc, 4 desc, 2 desc
),
--
cohort as (
-- New Users
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'1. New Users' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders_acc is null
group by 1,2,3,4,5,6
-- Retained Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'2. Retained Users' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is not null
group by 1,2,3,4,5,6
-- Retained Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'2.1 Retained Users - New LW' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is not null
	and orders1 = orders_acc
group by 1,2,3,4,5,6
-- Retained Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'2.2 Retained Users - Retained LW' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is not null
	and orders2 is not null
group by 1,2,3,4,5,6
-- Retained Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'2.3 Retained Users - Reactivated LW' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is not null
	and orders2 is null
	and orders1 <> orders_acc
group by 1,2,3,4,5,6
-- Reactivated Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'3. Reactivated Users' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is null
	and orders_acc is not null
group by 1,2,3,4,5,6
-- Reactivated Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'3.1 Reactivated Users - Recently Active' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is null
	and orders_acc is not null
	and orders2 is not null
group by 1,2,3,4,5,6
-- Reactivated Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'3.2 Reactivated Users - Recently Inactive' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
	and orders1 is null
	and orders_acc is not null
	and orders2 is null
group by 1,2,3,4,5,6
-- Active Users
union all
select distinct
	city,
	calendar,
	brand_id,
	brand_name,
	is_exclusive,
	'0. Active Users' as user_type,
	count(distinct application_user_id) as users,
	count(distinct case when orders0 > 1 then application_user_id else null end) as repurchase_users,
	sum(orders0) as orders,
	sum(gmv0) as gmv
from base
where 1=1
group by 1,2,3,4,5,6
)
--
select 
	c0.city,
	c0.calendar,
	c0.brand_id,
	c0.brand_name,
	c0.is_exclusive,
	-- Total Users
	sum(case when c0.user_type = '0. Active Users' then c0.users else 0 end) as active_users,
	sum(case when c0.user_type = '0. Active Users' then c0.orders else 0 end) as total_orders,
	sum(case when c0.user_type = '0. Active Users' then c0.gmv else 0 end) as total_gmv,
	-- Total Retention Rate
	sum(case when c0.user_type = '2. Retained Users' then c0.users else 0 end) as retained_users,
	sum(case when c1.user_type = '0. Active Users' then c1.users else 0 end) as base_users,
	-- Acquisition Retention Rate
	sum(case when c0.user_type = '2.1 Retained Users - New LW' then c0.users else 0 end) as new_users_retained,
	sum(case when c2.user_type = '1. New Users' then c2.users else 0 end) as base_new_users,
	-- Retained Retention Rate
	sum(case when c0.user_type = '2.2 Retained Users - Retained LW' then c0.users else 0 end) as retained_users_retained,
	sum(case when c3.user_type = '2. Retained Users' then c3.users else 0 end) as base_retained_users,
	-- Reactivation Retention Rate
	sum(case when c0.user_type = '2.3 Retained Users - Reactivated LW' then c0.users else 0 end) as reactivated_users_retained,
	sum(case when c4.user_type = '3. Reactivated Users' then c4.users else 0 end) as base_reactivated_users
--
from cohort c0
left join cohort c1 on c1.city = c0.city and c1.brand_id = c0.brand_id and c1.calendar = c0.calendar - interval '1 month' and c0.user_type = '2. Retained Users' and c1.user_type = '0. Active Users'
left join cohort c2 on c2.city = c0.city and c2.brand_id = c0.brand_id and c2.calendar = c0.calendar - interval '1 month' and c0.user_type = '2.1 Retained Users - New LW' and c2.user_type = '1. New Users'
left join cohort c3 on c3.city = c0.city and c3.brand_id = c0.brand_id and c3.calendar = c0.calendar - interval '1 month' and c0.user_type = '2.2 Retained Users - Retained LW' and c3.user_type = '2. Retained Users'
left join cohort c4 on c4.city = c0.city and c4.brand_id = c0.brand_id and c4.calendar = c0.calendar - interval '1 month' and c0.user_type = '2.3 Retained Users - Reactivated LW'  and c4.user_type = '3. Reactivated Users'
where 1=1
	and c0.calendar >= dateadd(month, -12, date_trunc(month, dateadd(day, -1, current_date)))
group by 1,2,3,4,5
having 1=1
	and c0.calendar <> date_trunc(month, dateadd(day, -1, current_date))
order by 2 desc, 1 asc, 6 desc